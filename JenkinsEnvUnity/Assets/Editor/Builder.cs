﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Mame{
class Builder
{
    // Output Path
    public static readonly string OUTPUT_PATH_WIN = "BUILD_WINDOW";    
    public static readonly string OUTPUT_PATH_ANDROID = "BUILD_ANDROID";
    public static readonly string OUTPUT_PATH_IOS = "BUILD_IOS";


    // Porject Name
    public static readonly string FILE_NAME_WIN = "Neoguri.exe";
    public static readonly string FILE_NAME_ANDROID = "Neoguri.apk";
    public static readonly string FILE_NAME_IOS = "Neoguri";

    // Scripting Define Symbols
    public static readonly string DEFINE_SYMBOL_WIN = "LOCAL_BUILD";
    public static readonly string DEFINE_SYMBOL_ANDROID = "LOCAL_BUILD";
    public static readonly string DEFINE_SYMBOL_IOS = "LOCAL_BUILD";
    

    static void PerformBuild()
    {
        string[] scenes = {"Assets/Scenes/SampleScene"};
        BuildPipeline.BuildPlayer(scenes,"neoguri.apk", BuildTarget.Android, BuildOptions.None);
    }


    [MenuItem("Build/Build/StandAlone(Win)")]
    static void PerformStandAloneBuildWindow()
    {
        try
        {
            //string[] args = Environment.GetCommandLineArgs();
            DeleteDirectory(OUTPUT_PATH_WIN);
            Thread.Sleep(100);
            CreateDirectory(OUTPUT_PATH_WIN);

            BuildOptions buildOptions = BuildOptions.None;
            List<string> sceneNames = GetSceneNames();
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, DEFINE_SYMBOL_WIN);
            BuildPipeline.BuildPlayer(sceneNames.ToArray(), Path.Combine(OUTPUT_PATH_WIN, FILE_NAME_WIN), BuildTarget.StandaloneWindows, buildOptions);            
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogError(e.ToString() + e.StackTrace);
            throw (e);
        }
    }

    [MenuItem("Build/Build/Android")]
    static void PerformAndroidBuild()
    {
        try
        {
            //string[] args = Environment.GetCommandLineArgs();
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
            DeleteDirectory(OUTPUT_PATH_ANDROID);
            Thread.Sleep(100);
            CreateDirectory(OUTPUT_PATH_ANDROID);

            BuildOptions buildOptions = BuildOptions.None;
            List<string> sceneNames = GetSceneNames();
        
            //RunAndroidKeyStoreSetting();
            PlayerSettings.statusBarHidden = true;
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, DEFINE_SYMBOL_ANDROID);       
            BuildPipeline.BuildPlayer(sceneNames.ToArray(), Path.Combine(OUTPUT_PATH_ANDROID, FILE_NAME_ANDROID), BuildTarget.Android, buildOptions);
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogError(e.ToString() + e.StackTrace);
            throw (e);
        }
       
    }

    [MenuItem("Build/Build/IOS")]
    static void PerformIOSBuild()
    {  
        try
        {
            // string[] args = Environment.GetCommandLineArgs();
            DeleteDirectory(OUTPUT_PATH_IOS);
            Thread.Sleep(100);
            CreateDirectory(OUTPUT_PATH_IOS);
            List<string> sceneNames = GetSceneNames();
            BuildOptions buildOptions = BuildOptions.SymlinkLibraries |
                                        BuildOptions.Development | 
                                        BuildOptions.ConnectWithProfiler |
                                        BuildOptions.AllowDebugging;

            PlayerSettings.iOS.sdkVersion = iOSSdkVersion.DeviceSDK;
            PlayerSettings.iOS.targetOSVersion =  iOSTargetOSVersion.iOS_8_0;
            PlayerSettings.statusBarHidden = true;                                      
                                        
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, DEFINE_SYMBOL_IOS);
            BuildPipeline.BuildPlayer(sceneNames.ToArray(), OUTPUT_PATH_IOS, BuildTarget.iOS, buildOptions);
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogError(e.ToString() + e.StackTrace);
            throw (e);
        }
    }


    static void RunAndroidKeyStoreSetting()
    {        
        PlayerSettings.companyName = "com.smilegate.com";
        PlayerSettings.productName = "Neoguri";
        PlayerSettings.applicationIdentifier = "com.neoguri.test";
        PlayerSettings.bundleVersion = "1.0";
        PlayerSettings.Android.bundleVersionCode = 1;

        PlayerSettings.Android.keystoreName = "user.keystore";
        PlayerSettings.Android.keystorePass = "123456";

        PlayerSettings.Android.keyaliasName = "neoguri";
        PlayerSettings.Android.keyaliasPass = "123456";
    }
    
    public static List<string> GetSceneNames()
    {
        List<string> sceneNames = new List<string>();
        for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
        {
            EditorBuildSettingsScene scene = EditorBuildSettings.scenes[i];
            if (scene.enabled)
            {
                sceneNames.Add(scene.path);
            }
        }
        return sceneNames;
    }


    public static void FindScene(DirectoryInfo dirInfo, List<FileInfo> sceneFiles)
    {
        foreach (FileInfo fileInfo in dirInfo.GetFiles())
        {
            if (fileInfo.Extension.Equals(".unity"))
                sceneFiles.Add(fileInfo);
        }
        foreach (DirectoryInfo dir in dirInfo.GetDirectories())
        {
            FindScene(dir, sceneFiles);
        }
    }


    public static void CreateDirectory(string dir)
    {
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
    }

    public static void DeleteDirectory(string dir)
    {
        if (Directory.Exists(dir))
        {
            DirectoryInfo sourceInfo = new DirectoryInfo(dir);
            DeleteDirectory(sourceInfo);
        }
    }

    public static void DeleteDirectory(DirectoryInfo directoryInfo)
    {
        FileInfo[] sourceFiles = directoryInfo.GetFiles();

        for (int i = 0; i < sourceFiles.Length; i++)
        {
            FileInfo file = sourceFiles[i];
            file.Delete();
        }

        DirectoryInfo[] subDirectories = directoryInfo.GetDirectories();
        for (int i = 0; i < subDirectories.Length; i++)
        {
            DirectoryInfo subDirectory = subDirectories[i];
            DeleteDirectory(subDirectory);
        }

        directoryInfo.Delete();
    }

    public static void CopyFile(string sourceFilePath, string destDirectory)
    {
        if (System.IO.File.Exists(sourceFilePath))
        {
            System.IO.File.Copy(@sourceFilePath, @destDirectory, true);
        }
    }

    public static void DeleteFile(string sourceFilePath)
    {
        if (System.IO.File.Exists(sourceFilePath))
        {
            System.IO.File.Delete(@sourceFilePath);
        }
    }

    public static void CopyDirectories(string sourceDirectory, string destDirectory)
    {
        CreateDirectory(destDirectory);

        DirectoryInfo sourceInfo = new DirectoryInfo(sourceDirectory);
        DirectoryInfo destInfo = new DirectoryInfo(destDirectory);

        CopyDirectories(sourceInfo, destInfo);
    }

    public static void CopyDirectories(DirectoryInfo sourceDirectory, DirectoryInfo destDirectory)
    {
        FileInfo[] sourceFiles = sourceDirectory.GetFiles();

        for (int i = 0; i < sourceFiles.Length; i++)
        {
            FileInfo file = sourceFiles[i];
            file.CopyTo(destDirectory.FullName + "/" + file.Name, true);
        }

        DirectoryInfo[] sourceSubDirectories = sourceDirectory.GetDirectories();

        for (int i = 0; i < sourceSubDirectories.Length; i++)
        {
            DirectoryInfo subDirectory = sourceSubDirectories[i];
            CopyDirectories(subDirectory.FullName, destDirectory.FullName + "/" + subDirectory.Name);
        }
    }

    public static void DeleteFilesSubName(string dir, string subName)
    {
        DirectoryInfo sourceInfo = new DirectoryInfo(dir);

        FileInfo[] sourceFiles = sourceInfo.GetFiles();

        for (int i = 0; i < sourceFiles.Length; i++)
        {
            FileInfo file = sourceFiles[i];
            if (file.Name.Contains(subName))
            {
                file.Delete();
            }
        }
    }

    public static void DeleteFilesInDir(string dir, string name)
    {
        DirectoryInfo sourceInfo = new DirectoryInfo(dir);

        FileInfo[] sourceFiles = sourceInfo.GetFiles();

        for (int i = 0; i < sourceFiles.Length; i++)
        {
            FileInfo file = sourceFiles[i];
            if (file.Name.Equals(name))
            {
                file.Delete();
            }
        }
    }

    public static void RenameFilesInDir(string dir, string name, string rename)
    {
        DirectoryInfo sourceInfo = new DirectoryInfo(dir);

        FileInfo[] sourceFiles = sourceInfo.GetFiles();

        for (int i = 0; i < sourceFiles.Length; i++)
        {
            FileInfo file = sourceFiles[i];

            if (file.Name.Equals(name))
            {
                if (File.Exists(dir + "/" + rename))
                {
                    File.Delete(dir + "/" + rename);
                    Thread.Sleep(10);
                }
                file.MoveTo(dir + "/" + rename);
            }
        }
    }


}
}